﻿anb_marked_by_destiny_modifier = {
    icon = magic_positive
	health = 0.75
	fertility = 0.25
	intrigue_scheme_resistance = 10
}

arbarani_cultural_settlement = {
	icon = county_modifier_development_positive	
	development_growth_factor = 0.20
}

westmoors_subjugator_modifier = { #Unlocks cb vs moorlands.
	icon = prestige_positive
}