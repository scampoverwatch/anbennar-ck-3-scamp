﻿
lorenti = {
	color = "lorentish_green"

	ethos = ethos_communal
	heritage = heritage_lencori
	language = language_lencori_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_horse_breeder
		tradition_culinary_art
		tradition_storytellers
		tradition_equal_inheritance
		tradition_chanson_de_geste # Needs to be reflavoured to Lorentish name/desc
	}
	
	name_list = name_list_lorenti
	
	coa_gfx = { frankish_group_coa_gfx western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { western_clothing_gfx }
	unit_gfx = { western_unit_gfx }

	ethnicities = {
		10 = caucasian_blond
		5 = caucasian_ginger
		45 = caucasian_brown_hair
		35 = caucasian_dark_hair
	}
}

lorentish = {
	color = "lorentish_red"
	created = 1000.1.1			#placeholder for a more accurate date
	parents = { lorenti moon_elvish }

	ethos = ethos_courtly
	heritage = heritage_lencori
	language = language_lencori_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_martial_admiration
		tradition_culinary_art
		tradition_astute_diplomats
		tradition_equal_inheritance
		tradition_chanson_de_geste # Needs to be reflavoured to Lorentish name/desc
	}
	
	name_list = name_list_lorentish
	
	coa_gfx = { french_coa_gfx western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { western_clothing_gfx }
	unit_gfx = { western_unit_gfx }

	ethnicities = {
		5 = caucasian_blond
		30 = caucasian_ginger
		60 = caucasian_brown_hair
		5 = caucasian_dark_hair
	}
}

roilsardi = {
	color = "roilsardi_red"

	ethos = ethos_bellicose
	heritage = heritage_lencori
	language = language_lencori_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_family_entrepreneurship
		tradition_formation_fighting
		tradition_quarrelsome
	}
	
	name_list = name_list_roilsardi
	
	coa_gfx = { french_coa_gfx western_coa_gfx }
	building_gfx = { mediterranean_building_gfx }
	clothing_gfx = { western_clothing_gfx }
	unit_gfx = { western_unit_gfx }

	ethnicities = {
		20 = caucasian_ginger
		65 = caucasian_brown_hair
		15 = caucasian_dark_hair
	}
}

derannic = {
	color = "derannic_pink"
	created = 860.1.1	#canon
	parents = { entebenic olavish }

	ethos = ethos_courtly
	heritage = heritage_lencori
	language = language_lencori_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_chivalry
		tradition_horse_breeder
		tradition_seafaring
	}
	
	name_list = name_list_derannic
	
	coa_gfx = { norman_coa_gfx western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { western_clothing_gfx }
	unit_gfx = { western_unit_gfx }

	ethnicities = {
		25 = caucasian_blond
		10 = caucasian_ginger
		30 = caucasian_brown_hair
		15 = caucasian_northern_blond
		5 = caucasian_northern_ginger
		15 = caucasian_northern_brown_hair
	}
}

sorncosti = {
	color = { 135 3 86 }
	created = 1020.1.1			#placeholder for a more accurate date
	parents = { sormanni moon_elvish }

	ethos = ethos_stoic
	heritage = heritage_lencori
	language = language_lencori_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_hill_dwellers
		tradition_eye_for_an_eye
		tradition_culinary_art
	}
	dlc_tradition = {
		trait = tradition_artisans
		requires_dlc_flag = royal_court
	}
	
	name_list = name_list_sorncosti
	
	coa_gfx = { occitan_coa_gfx western_coa_gfx }
	building_gfx = { mediterranean_building_gfx }
	clothing_gfx = { western_clothing_gfx }
	unit_gfx = { western_unit_gfx }

	ethnicities = {
		10 = caucasian_blond
		5 = caucasian_ginger
		35 = caucasian_brown_hair
		50 = caucasian_dark_hair
	}
}

sormanni = {
	color = { 81 2 52 }

	ethos = ethos_stoic
	heritage = heritage_lencori
	language = language_old_lencori
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_hill_dwellers
		tradition_isolationist
		tradition_culinary_art
		tradition_metal_craftsmanship
	}
	
	name_list = name_list_sormanni
	
	coa_gfx = { occitan_coa_gfx western_coa_gfx }
	building_gfx = { mediterranean_building_gfx }
	clothing_gfx = { western_clothing_gfx }
	unit_gfx = { western_unit_gfx }

	ethnicities = {
		50 = caucasian_brown_hair
		50 = caucasian_dark_hair
	}
}

iochander = {
	color = { 173 61 135 }
	created = 474.1.1
	parents = { lorenti creek_gnomish }
	
	ethos = ethos_courtly
	heritage = heritage_lencori
	language = language_gnomish
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_loyal_soldiers
		tradition_language_scholars
		tradition_language_gnomish_lencori
		tradition_agrarian
	}
	
	name_list = name_list_iochander
	
	coa_gfx = { occitan_coa_gfx western_coa_gfx }
	building_gfx = { mediterranean_building_gfx }
	clothing_gfx = { western_clothing_gfx }
	unit_gfx = { western_unit_gfx }

	ethnicities = {
		30 = caucasian_blond
		10 = caucasian_ginger
		50 = caucasian_brown_hair
		10 = caucasian_dark_hair
	}
}

entebenic = {
	color = { 120 20 20}
	
	ethos = ethos_egalitarian
	heritage = heritage_lencori
	language = language_lencori_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_horse_breeder
		tradition_chivalry
		tradition_family_entrepreneurship
		tradition_hereditary_hierarchy
		tradition_chanson_de_geste # Needs to be reflavoured to Lorentish name/desc
	}

	name_list = name_list_entebenic
	
	coa_gfx = { frankish_group_coa_gfx western_coa_gfx }
	building_gfx = { mediterranean_building_gfx }
	clothing_gfx = { western_clothing_gfx }
	unit_gfx = { western_unit_gfx }
	
	ethnicities = {
		20 = caucasian_blond
		10 = caucasian_ginger
		55 = caucasian_brown_hair
		15 = caucasian_dark_hair
	}
}

crovanni = {
	color = { 217 109 98 }
	created = 62.1.1
	parents = { sormanni }
	
	ethos = ethos_courtly
	heritage = heritage_lencori
	language = language_lencori_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_fishermen
		tradition_isolationist
		tradition_chivalry
		tradition_faith_bound
	}
	
	name_list = name_list_crovanni
	
	coa_gfx = { occitan_coa_gfx western_coa_gfx }
	building_gfx = { mediterranean_building_gfx }
	clothing_gfx = { western_clothing_gfx }
	unit_gfx = { western_unit_gfx }

	ethnicities = {
		 5 = caucasian_ginger
		45 = caucasian_brown_hair
		50 = caucasian_dark_hair
	}
}