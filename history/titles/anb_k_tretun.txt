k_tretun = {
	1000.1.1 = { change_development_level = 8 }
}

c_tretun = {
	1000.1.1 = { change_development_level = 9 }
}

d_roilsard = {
	1000.1.1 = { change_development_level = 11 }
	872.1.1 = {
		holder = roilsardis_0001
	}
	906.7.19 = {
		holder = roilsardis_0002
	}
	915.10.11 = {
		holder = roilsardis_0005
	}
	951.12.1 = {
		holder = roilsardis_0008
	}
	979.5.29 = {
		holder = 0
	}
}

c_roilsard = {
	1001.1.1 = { change_development_level = 12 }
	979.5.29 = {
		holder = roilsardis_0016
	}
	982.8.14 = {
		holder = 504
	}
	1005.4.30 = {
		holder = 6	#Gregoire Roilsard
	}
}

c_saloren = {
	998.3.23 = {
		holder = 501	#Artur síl Saloren
	}
}

c_vivinmar = {
	979.5.29 = {
		holder = roilsardis_0010
	}
	989.4.15 = {
		holder = 508 #Marcel II síl Vivin
	}
	1001.1.12 = {
		holder = 4	#Petrus sil Vivin
	}
}


c_loopuis = {
	979.5.29 = [
		holder = roilsardis_0013
	}
	993.6.12 = {
		holder = 5	#Caylen sil na Loop
	}
}